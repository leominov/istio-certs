package cmd

import (
	"log"
	"os"
	"path/filepath"
	"sync"

	"github.com/spf13/cobra"

	"gitlab-rnd.tcsbank.ru/devops/k8s-tools/istio-certs/pkg/istiocerts"
	"gitlab-rnd.tcsbank.ru/devops/k8s-tools/istio-certs/pkg/istiocertsk8s"
)

func init() {
	fillCmd.PersistentFlags().StringVarP(
		&certificatesConfigFile,
		"certificates",
		"f",
		"certificates.yaml",
		"certificates file with certificates definition, must be YAML",
	)

	fillCmd.PersistentFlags().StringVarP(
		&namespace,
		"namespace",
		"n",
		"istio-system",
		"kubernetes namespace where secrets have been created",
	)

	fillCmd.PersistentFlags().StringVarP(
		&kubeConfig,
		"kubeconfig",
		"k",
		filepath.Join(os.Getenv("HOME"), ".kube", "config"),
		"Path to kubeconfig",
	)

	rootCmd.AddCommand(fillCmd)
}

var (
	certificatesConfigFile string
	namespace              string
	kubeConfig             string

	fillCmd = &cobra.Command{
		Use:   "create-secrets",
		Short: "Create secrets from certificates",
		Run: func(_ *cobra.Command, _ []string) {
			config, err := istiocerts.NewConfigFromFile(certificatesConfigFile)

			if err != nil {
				log.Fatalf("Cannot read config file: %v", err)
			}

			writer, err := istiocertsk8s.NewK8sSecretWriter(kubeConfig, namespace)
			if err != nil {
				log.Fatalf("Failed to create kubernetes client: %v", err)
			}

			provider, err := istiocerts.NewCertificateProvider(config)
			if err != nil {
				log.Fatalf("Failed to create certificate provider: %v", err)
			}

			wg := &sync.WaitGroup{}
			wg.Add(len(config.CPs))

			hasFailedOperations := false
			for _, cp := range config.CPs {
				go func(cp istiocerts.CertificatePath) {
					defer wg.Done()
					err := createCertificate(cp, provider, writer)
					if err != nil {
						hasFailedOperations = true
					}
				}(cp)
			}

			wg.Wait()

			// Выходим с ненулевым кодом при наличии ошибок, чтобы зафэйлить
			// шаг, выполняемый в CI
			if hasFailedOperations {
				log.Fatal("Failed. See logs above")
			}
		},
	}
)

func createCertificate(
	path istiocerts.CertificatePath,
	provider istiocerts.CertificateProvider,
	writer istiocerts.CertificateWriter,
) error {
	log.Printf("Certificate - %s. Started processing", path.Fqdn)
	defer log.Printf("Certificate - %s. Completed processing", path.Fqdn)

	cert, err := istiocerts.NewCertificate(provider, path)
	if err != nil {
		log.Printf("Certificate - %s. Failed to read from %s: %v", path.Fqdn, provider, err)
		return err
	}
	log.Printf("Certificate - %s. Readed from %s", path.Fqdn, provider)

	metadata, err := cert.Verify()
	if err != nil {
		log.Printf("Certificate - %s. Verify failed: %v", path.Fqdn, err)
		return err
	}

	if len(metadata.Version) == 0 {
		log.Printf("Certificate - %s. Version not specified", path.Fqdn)
	}

	log.Printf("Certificate - %s. Verify completed", path.Fqdn)

	err = cert.WriteSecret(writer)
	if err != nil {
		log.Printf("Certificate - %s. Failed to write certificate: %v", path.Fqdn, err)
		return err
	}

	log.Printf("Certificate - %s. Writed", path.Fqdn)
	return nil
}
