package cmd

import (
	"fmt"
	"log"
	"time"

	"github.com/spf13/cobra"

	"gitlab-rnd.tcsbank.ru/devops/k8s-tools/istio-certs/pkg/istiocerts"
)

const (
	validUntilFormat = "2006-01-02"
)

var (
	verifyTimeShift time.Duration

	verifyCertificatesCmd = &cobra.Command{
		Use:   "verify-certificates",
		Short: "Verify certificates",
		RunE:  verifyCertificates,
	}
)

func init() {
	verifyCertificatesCmd.PersistentFlags().StringVarP(
		&certificatesConfigFile,
		"certificates",
		"f",
		"certificates.yaml",
		"certificates file with certificates definition, must be YAML",
	)

	verifyCertificatesCmd.PersistentFlags().DurationVarP(
		&verifyTimeShift,
		"timeshift",
		"t",
		0,
		"time shift for certificate verification (ex.: 30s, 15m, 24h15m, 168h)",
	)

	rootCmd.AddCommand(verifyCertificatesCmd)
}

func verifyCertificates(_ *cobra.Command, _ []string) error {
	config, err := istiocerts.NewConfigFromFile(certificatesConfigFile)
	if err != nil {
		return fmt.Errorf("cannot read config file: %v", err)
	}

	provider, err := istiocerts.NewCertificateProvider(config)
	if err != nil {
		return fmt.Errorf("failed to create certificate provider: %v", err)
	}

	for _, cp := range config.CPs {
		log.Printf("Certificate - %s. Processing", cp.Fqdn)
		meta, err := verifyCertificate(provider, cp)
		if err != nil {
			return err
		}
		validUntil := meta.NotAfter.Format(validUntilFormat)
		log.Printf("Certificate - %s. Valid until %s", cp.Fqdn, validUntil)
	}

	return nil
}

func verifyCertificate(provider istiocerts.CertificateProvider, cp istiocerts.CertificatePath) (istiocerts.CertificateMetadata, error) {
	meta := istiocerts.CertificateMetadata{}

	cert, err := istiocerts.NewCertificate(provider, cp)
	if err != nil {
		return meta, fmt.Errorf("failed to read %s certificate from %s: %v", cp.Fqdn, provider, err)
	}

	meta, err = cert.Verify(
		istiocerts.WithTimeShift(verifyTimeShift),
	)
	if err != nil {
		return meta, fmt.Errorf("verify failed for %s certificate: %v", cp.Fqdn, err)
	}

	return meta, nil
}
