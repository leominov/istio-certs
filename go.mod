module gitlab-rnd.tcsbank.ru/devops/k8s-tools/istio-certs

go 1.14

require (
	github.com/hashicorp/vault/api v1.0.4
	github.com/spf13/cobra v1.0.0
	github.com/stretchr/testify v1.4.0
	gopkg.in/yaml.v2 v2.2.8
	k8s.io/api v0.19.2
	k8s.io/apimachinery v0.19.2
	k8s.io/client-go v0.19.2
)
