package main

import (
	"gitlab-rnd.tcsbank.ru/devops/k8s-tools/istio-certs/cmd"
)

func main() {
	cmd.Execute()
}
