package istiocertsk8s

import (
	"context"
	"fmt"
	"log"
	"strings"

	v12 "k8s.io/api/core/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
	"k8s.io/client-go/tools/clientcmd"

	"gitlab-rnd.tcsbank.ru/devops/k8s-tools/istio-certs/pkg/istiocerts"
)

const (
	managedByLabel       = "devops-advert_istio-certs"
	sourceCodeAnnotation = "gitlab-rnd.tcsbank.ru/devops/k8s-tools/istio-certs"
)

type K8sSecretWriter struct {
	cs kubernetes.Interface
	ns string
}

func NewK8sSecretWriter(kubeConfig, namespace string) (*K8sSecretWriter, error) {
	config, err := clientcmd.BuildConfigFromFlags("", kubeConfig)
	if err != nil {
		return nil, err
	}
	clientSet, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, err
	}

	return &K8sSecretWriter{cs: clientSet, ns: namespace}, nil
}

func (ksw K8sSecretWriter) Write(metadata istiocerts.CertificateMetadata, public, private []byte) error {
	commonName := strings.ReplaceAll(metadata.CommonName, "*", "wildcard")

	name := fmt.Sprintf("%s-%s",
		commonName,
		metadata.NotAfter.Format("20060102"),
	)

	labelMap := map[string]string{
		"common-name": commonName,
		"not-after":   metadata.NotAfter.Format("2006-01-02T15.04.05"),
		"managed-by":  managedByLabel,
	}

	if len(metadata.Version) > 0 {
		labelMap["version"] = metadata.Version
	}

	annotationMap := map[string]string{
		"source-code": sourceCodeAnnotation,
	}

	secretSpec := &v12.Secret{
		Type: v12.SecretTypeTLS,
		ObjectMeta: v1.ObjectMeta{
			Name:        name,
			Namespace:   ksw.ns,
			Labels:      labelMap,
			Annotations: annotationMap,
		},
		Data: map[string][]byte{
			"tls.crt": public,
			"tls.key": private,
		},
	}

	// Проверяем наличие сертификата в кластере, если не найден - создаем
	clusterSecret, err := ksw.cs.CoreV1().Secrets(ksw.ns).Get(context.TODO(), name, v1.GetOptions{})
	if err != nil {
		secret, err := ksw.cs.CoreV1().Secrets(ksw.ns).Create(context.TODO(), secretSpec, v1.CreateOptions{})
		if err != nil {
			return err
		}
		log.Printf("Secret %s in namespace %s created", secret.Name, secret.Namespace)
		return nil
	}

	// При указании точной версии нужно убедиться, что на сервере создан секрет
	// сертификата на основе нужной версии секрета Vault, если на сервере будет
	// найден секрет без указания версии, либо не той, что нам нужно - обновляем
	if len(metadata.Version) == 0 {
		log.Printf("Secret %s in namespace %s already exists", secretSpec.Name, secretSpec.Namespace)
		return nil
	}

	// Если версия секрета в кластере задана и такая же, что нам нужно - пропускаем,
	// в противном случае - обновляем до нужной версии
	version, ok := clusterSecret.Labels["version"]
	if ok && strings.Compare(version, metadata.Version) == 0 {
		log.Printf("Secret %s in namespace %s already exists", secretSpec.Name, secretSpec.Namespace)
		return nil
	}

	secret, err := ksw.cs.CoreV1().Secrets(ksw.ns).Update(context.TODO(), secretSpec, v1.UpdateOptions{})
	if err != nil {
		return err
	}

	if ok {
		log.Printf("Secret %s in namespace %s updated from %s to %s version",
			secret.Name, secret.Namespace, version, metadata.Version)
	} else {
		log.Printf("Secret %s in namespace %s updated to %s version",
			secret.Name, secret.Namespace, metadata.Version)
	}

	return nil
}
