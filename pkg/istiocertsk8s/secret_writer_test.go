package istiocertsk8s

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	v12 "k8s.io/api/core/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes/fake"

	"gitlab-rnd.tcsbank.ru/devops/k8s-tools/istio-certs/pkg/istiocerts"
)

func TestK8sSecretWriter_Write_HappyPath(t *testing.T) {
	cli := fake.NewSimpleClientset()
	writer := K8sSecretWriter{
		cs: cli,
		ns: "default",
	}
	err := writer.Write(istiocerts.CertificateMetadata{
		CommonName: "foobar.local",
		NotAfter:   time.Date(2020, 1, 15, 3, 15, 0, 0, time.UTC),
	}, []byte(`public`), []byte(`private`))
	assert.NoError(t, err)

	secret, err := cli.CoreV1().Secrets("default").Get(context.TODO(), "foobar.local-20200115", v1.GetOptions{})
	if !assert.NoError(t, err) {
		return
	}

	for name, val := range map[string]string{
		"common-name": "foobar.local",
		"not-after":   "2020-01-15T03.15.00",
		"managed-by":  managedByLabel,
	} {
		res, ok := secret.Labels[name]
		if assert.True(t, ok) {
			assert.Equal(t, val, res)
		}
	}

	_, ok := secret.Labels["version"]
	assert.False(t, ok)

	for name, val := range map[string][]byte{
		"tls.crt": []byte(`public`),
		"tls.key": []byte(`private`),
	} {
		res, ok := secret.Data[name]
		if assert.True(t, ok) {
			assert.Equal(t, val, res)
		}
	}
}

func TestK8sSecretWriter_Write_AlreadyExist_WithoutVersion(t *testing.T) {
	cli := fake.NewSimpleClientset(&v12.Secret{
		Type: v12.SecretTypeTLS,
		ObjectMeta: v1.ObjectMeta{
			Name:      "wildcard.foobar.local-20200115",
			Namespace: "istio-system",
			Labels: map[string]string{
				"version": "1",
			},
		},
		Data: map[string][]byte{
			"tls.crt": []byte(`public_v1`),
			"tls.key": []byte(`private_v1`),
		},
	})
	writer := K8sSecretWriter{
		cs: cli,
		ns: "istio-system",
	}
	err := writer.Write(istiocerts.CertificateMetadata{
		Version:    "1",
		CommonName: "*.foobar.local",
		NotAfter:   time.Date(2020, 1, 15, 3, 15, 0, 0, time.UTC),
	}, []byte(`public_v2`), []byte(`private_v2`))
	assert.NoError(t, err)

	secret, err := cli.CoreV1().Secrets("istio-system").Get(context.TODO(), "wildcard.foobar.local-20200115", v1.GetOptions{})
	if !assert.NoError(t, err) {
		return
	}
	for name, val := range map[string][]byte{
		"tls.crt": []byte(`public_v1`),
		"tls.key": []byte(`private_v1`),
	} {
		res, ok := secret.Data[name]
		if assert.True(t, ok) {
			assert.Equal(t, val, res)
		}
	}
}

func TestK8sSecretWriter_Write_AlreadyExist_WithVersion(t *testing.T) {
	cli := fake.NewSimpleClientset(&v12.Secret{
		Type: v12.SecretTypeTLS,
		ObjectMeta: v1.ObjectMeta{
			Name:      "wildcard.foobar.local-20200115",
			Namespace: "istio-system",
		},
		Data: map[string][]byte{
			"tls.crt": []byte(`public_v1`),
			"tls.key": []byte(`private_v1`),
		},
	})
	writer := K8sSecretWriter{
		cs: cli,
		ns: "istio-system",
	}
	err := writer.Write(istiocerts.CertificateMetadata{
		CommonName: "*.foobar.local",
		NotAfter:   time.Date(2020, 1, 15, 3, 15, 0, 0, time.UTC),
	}, []byte(`public_v2`), []byte(`private_v2`))
	assert.NoError(t, err)

	secret, err := cli.CoreV1().Secrets("istio-system").Get(context.TODO(), "wildcard.foobar.local-20200115", v1.GetOptions{})
	if !assert.NoError(t, err) {
		return
	}
	_, ok := secret.Labels["version"]
	assert.False(t, ok)
	for name, val := range map[string][]byte{
		"tls.crt": []byte(`public_v1`),
		"tls.key": []byte(`private_v1`),
	} {
		res, ok := secret.Data[name]
		if assert.True(t, ok) {
			assert.Equal(t, val, res)
		}
	}
}

func TestK8sSecretWriter_Write_Update_WithoutVersion(t *testing.T) {
	cli := fake.NewSimpleClientset(&v12.Secret{
		Type: v12.SecretTypeTLS,
		ObjectMeta: v1.ObjectMeta{
			Name:      "wildcard.foobar.local-20200115",
			Namespace: "istio-system",
		},
		Data: map[string][]byte{
			"tls.crt": []byte(`public_v1`),
			"tls.key": []byte(`private_v1`),
		},
	})
	writer := K8sSecretWriter{
		cs: cli,
		ns: "istio-system",
	}
	err := writer.Write(istiocerts.CertificateMetadata{
		Version:    "2",
		CommonName: "*.foobar.local",
		NotAfter:   time.Date(2020, 1, 15, 3, 15, 0, 0, time.UTC),
	}, []byte(`public_v2`), []byte(`private_v2`))
	assert.NoError(t, err)

	secret, err := cli.CoreV1().Secrets("istio-system").Get(context.TODO(), "wildcard.foobar.local-20200115", v1.GetOptions{})
	if !assert.NoError(t, err) {
		return
	}
	version, ok := secret.Labels["version"]
	if assert.True(t, ok) {
		assert.Equal(t, "2", version)
	}
	for name, val := range map[string][]byte{
		"tls.crt": []byte(`public_v2`),
		"tls.key": []byte(`private_v2`),
	} {
		res, ok := secret.Data[name]
		if assert.True(t, ok) {
			assert.Equal(t, val, res)
		}
	}
}

func TestK8sSecretWriter_Write_Update_WithVersion(t *testing.T) {
	cli := fake.NewSimpleClientset(&v12.Secret{
		Type: v12.SecretTypeTLS,
		ObjectMeta: v1.ObjectMeta{
			Name:      "wildcard.foobar.local-20200115",
			Namespace: "istio-system",
			Labels: map[string]string{
				"version": "1",
			},
		},
		Data: map[string][]byte{
			"tls.crt": []byte(`public_v1`),
			"tls.key": []byte(`private_v1`),
		},
	})
	writer := K8sSecretWriter{
		cs: cli,
		ns: "istio-system",
	}
	err := writer.Write(istiocerts.CertificateMetadata{
		Version:    "2",
		CommonName: "*.foobar.local",
		NotAfter:   time.Date(2020, 1, 15, 3, 15, 0, 0, time.UTC),
	}, []byte(`public_v2`), []byte(`private_v2`))
	assert.NoError(t, err)

	secret, err := cli.CoreV1().Secrets("istio-system").Get(context.TODO(), "wildcard.foobar.local-20200115", v1.GetOptions{})
	if !assert.NoError(t, err) {
		return
	}
	version, ok := secret.Labels["version"]
	if assert.True(t, ok) {
		assert.Equal(t, "2", version)
	}
	for name, val := range map[string][]byte{
		"tls.crt": []byte(`public_v2`),
		"tls.key": []byte(`private_v2`),
	} {
		res, ok := secret.Data[name]
		if assert.True(t, ok) {
			assert.Equal(t, val, res)
		}
	}
}
