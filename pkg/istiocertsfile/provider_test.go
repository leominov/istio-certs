package istiocertsfile

import "testing"

func TestGetCert(t *testing.T) {
	dir := "../../test_data/tls/pfa/adm_20190508"
	p, _ := New()
	_, err := p.GetCert(dir)
	if err != nil {
		t.Errorf("GetCert(): %v", err)
	}
	_, err = p.GetKey(dir)
	if err != nil {
		t.Errorf("GetKey(): %v", err)
	}
	_, err = p.GetCa(dir)
	if err != nil {
		t.Errorf("GetCa(): %v", err)
	}

	dir = "../../test_data/tls/pfa/adm_20190508_version#1"
	p, _ = New()
	_, err = p.GetCert(dir)
	if err != nil {
		t.Errorf("GetCert(): %v", err)
	}
	_, err = p.GetKey(dir)
	if err != nil {
		t.Errorf("GetKey(): %v", err)
	}
	_, err = p.GetCa(dir)
	if err != nil {
		t.Errorf("GetCa(): %v", err)
	}
}
