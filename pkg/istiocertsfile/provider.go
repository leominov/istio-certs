package istiocertsfile

import (
	"io/ioutil"
	"path"
	"strings"
)

type FileProvider struct{}

func New() (FileProvider, error) {
	return FileProvider{}, nil
}

func getVersionDir(dir string, file string) string {
	pair := strings.Split(dir, "#")
	if len(pair) > 1 {
		return path.Join(pair[0], "v"+pair[1], file)
	}
	return path.Join(pair[0], file)
}

func (f FileProvider) GetCert(dir string) ([]byte, error) {
	return ioutil.ReadFile(getVersionDir(dir, "crt"))
}

func (f FileProvider) GetKey(dir string) ([]byte, error) {
	return ioutil.ReadFile(getVersionDir(dir, "key"))
}

func (f FileProvider) GetCa(dir string) ([]byte, error) {
	return ioutil.ReadFile(getVersionDir(dir, "ca"))
}
