package istiocertsvault

import (
	"fmt"
	"os"
	"strings"

	"github.com/hashicorp/vault/api"
)

const (
	envAuthMethod = "VAULT_AUTH_METHOD"
	envToken      = "VAULT_TOKEN"
	envLogin      = "VAULT_LOGIN"
	envPassword   = "VAULT_PASSWORD"
	envMountPoint = "VAULT_MOUNT_POINT"

	vaultCertKey       = "crt"
	vaultPrivatekeyKey = "key"
	vaultCaKey         = "ca"
)

type VaultProvider struct {
	client *api.Client
}

func New() (VaultProvider, error) {
	cl, err := api.NewClient(api.DefaultConfig())
	if err != nil {
		return VaultProvider{}, fmt.Errorf("failed to create vault client: %v", err)
	}

	authMethod := os.Getenv(envAuthMethod)
	switch authMethod {
	case "token":
		cl.SetToken(os.Getenv(envToken))
		break
	case "ldap", "userpass":
		path := fmt.Sprintf("auth/%s/login/%s", authMethod, os.Getenv(envLogin))
		options := map[string]interface{}{"password": os.Getenv(envPassword)}
		secret, err := cl.Logical().Write(path, options)
		if err != nil {
			return VaultProvider{}, fmt.Errorf("vault auth failed: %v", err)
		}
		cl.SetToken(secret.Auth.ClientToken)
		break
	default:
		return VaultProvider{}, fmt.Errorf("unsupported authMethod: '%s'. Supported: token, userpass", authMethod)
	}

	return VaultProvider{client: cl}, nil
}

func (vp VaultProvider) GetCert(path string) ([]byte, error) {
	return vp.readSecretKey(path, vaultCertKey)
}

func (vp VaultProvider) GetKey(path string) ([]byte, error) {
	return vp.readSecretKey(path, vaultPrivatekeyKey)
}

func (vp VaultProvider) GetCa(path string) ([]byte, error) {
	return vp.readSecretKey(path, vaultCaKey)
}

func (vp VaultProvider) readSecret(path string) (map[string]interface{}, error) {
	var params map[string][]string

	pair := strings.Split(path, "#")
	if len(pair) > 1 {
		params = map[string][]string{
			"version": {
				pair[1],
			},
		}
	}

	p := fmt.Sprintf("%s/data/%s", os.Getenv(envMountPoint), pair[0])
	secret, err := vp.client.Logical().ReadWithData(p, params)
	if err != nil {
		return nil, err
	}

	if secret == nil {
		return nil, fmt.Errorf("secret %q not found", pair[0])
	}

	data, ok := secret.Data["data"]
	if !ok {
		return nil, fmt.Errorf("secret %q data not found", pair[0])
	}

	return data.(map[string]interface{}), nil
}

func (vp VaultProvider) validateData(data interface{}) []byte {
	if data == nil {
		return nil
	}

	switch data.(type) {
	case string:
		return []byte(data.(string))
	default:
		return data.([]byte)
	}
}

func (vp VaultProvider) readSecretKey(path, key string) ([]byte, error) {
	data, err := vp.readSecret(path)
	if err != nil {
		return nil, err
	}

	return vp.validateData(data[key]), nil
}

func (vp VaultProvider) String() string {
	return vp.client.Address()
}
