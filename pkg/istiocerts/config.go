package istiocerts

import (
	"io/ioutil"
	"strings"

	"gopkg.in/yaml.v2"
)

type CertificatePath struct {
	Fqdn string `yaml:"fqdn"`
	// Может содержать версию секрета, которая задается аналогично Kubernetes
	// интеграции с Vault. Например: tls/tj/wildcard_tinkoffjournal.ru_20210922#1
	// ref: https://www.vaultproject.io/api/secret/kv/kv-v2.html#read-secret-version
	Path string `yaml:"path"`
}

type Config struct {
	Source string            `yaml:"source"`
	CPs    []CertificatePath `yaml:"certificates"`
}

func NewConfigFromFile(filename string) (*Config, error) {
	yamlData, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	config := &Config{}
	err = yaml.Unmarshal(yamlData, config)
	return config, err
}

func (cp CertificatePath) Version() string {
	var version string
	if data := strings.Split(cp.Path, "#"); len(data) > 1 {
		version = data[1]
	}
	return version
}
