package istiocerts

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewConfigFromFile(t *testing.T) {
	_, err := NewConfigFromFile("../../test_data/config/valid.yaml")
	assert.NoError(t, err)

	_, err = NewConfigFromFile("../../test_data/config/notfound.yaml")
	assert.Error(t, err)

	_, err = NewConfigFromFile("../../test_data/config/invalid.yaml")
	assert.Error(t, err)
}

func TestCertificatePath_Version(t *testing.T) {
	cp := &CertificatePath{
		Path: "tls/tj/wildcard_tinkoffjournal.ru_20210922",
	}
	assert.Equal(t, "", cp.Version())

	cp = &CertificatePath{
		Path: "tls/tj/wildcard_tinkoffjournal.ru_20210922#",
	}
	assert.Equal(t, "", cp.Version())

	cp = &CertificatePath{
		Path: "tls/tj/wildcard_tinkoffjournal.ru_20210922#1",
	}
	assert.Equal(t, "1", cp.Version())
}
