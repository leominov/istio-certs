package istiocerts

import (
	"fmt"

	"gitlab-rnd.tcsbank.ru/devops/k8s-tools/istio-certs/pkg/istiocertsfile"
	"gitlab-rnd.tcsbank.ru/devops/k8s-tools/istio-certs/pkg/istiocertsvault"
)

type CertificateProvider interface {
	GetCert(path string) ([]byte, error)
	GetKey(path string) ([]byte, error)
	GetCa(path string) ([]byte, error)
}

func NewCertificateProvider(config *Config) (CertificateProvider, error) {
	switch config.Source {
	case "vault":
		return istiocertsvault.New()
	case "file":
		return istiocertsfile.New()
	default:
		return nil, fmt.Errorf("unknown provider: %s", config.Source)
	}
}
