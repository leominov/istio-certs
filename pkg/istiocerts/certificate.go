package istiocerts

import (
	"bytes"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"time"
)

type Certificate struct {
	fqdn     string
	metadata CertificateMetadata
	path     CertificatePath

	caData   []byte
	certData []byte
	keyData  []byte
}

type CertificateMetadata struct {
	CommonName string
	NotAfter   time.Time
	Version    string
}

func NewCertificate(provider CertificateProvider, cp CertificatePath) (*Certificate, error) {
	certData, err := provider.GetCert(cp.Path)
	if err != nil {
		return nil, err
	}

	keyData, err := provider.GetKey(cp.Path)
	if err != nil {
		return nil, err
	}

	caData, err := provider.GetCa(cp.Path)
	if err != nil {
		return nil, err
	}

	return &Certificate{
		caData:   caData,
		certData: certData,
		fqdn:     cp.Fqdn,
		keyData:  keyData,
		path:     cp,
	}, nil
}

func (c *Certificate) Verify(options ...VerifyOption) (CertificateMetadata, error) {
	b, rest := pem.Decode(c.certData)
	if b == nil {
		return c.metadata, errors.New("failed to find PEM block")
	}

	commonCrt, err := x509.ParseCertificate(b.Bytes)
	if commonCrt == nil {
		return c.metadata, errors.New("first pem block isn't corresponding to x509 certificate")
	}
	if err != nil {
		return c.metadata, err
	}

	c.metadata = CertificateMetadata{
		CommonName: commonCrt.Subject.CommonName,
		NotAfter:   commonCrt.NotAfter,
		Version:    c.path.Version(),
	}

	intermediatePool := x509.NewCertPool()
	rootPool := x509.NewCertPool()

	for {
		b, rest = pem.Decode(rest)
		if b == nil {
			break
		}

		crt, err := x509.ParseCertificate(b.Bytes)
		if err != nil {
			return c.metadata, err
		}

		if crt.IsCA {
			rootPool.AddCert(crt)
			continue
		}

		intermediatePool.AddCert(crt)
	}

	if len(c.caData) > 0 {
		rootPool.AppendCertsFromPEM(c.caData)
	}

	verifyOpts := x509.VerifyOptions{
		Roots:         rootPool,
		Intermediates: intermediatePool,
		DNSName:       c.fqdn,
	}

	for _, opt := range options {
		opt(&verifyOpts)
	}

	_, err = commonCrt.Verify(verifyOpts)

	return c.metadata, err
}

func (c Certificate) WriteSecret(writer CertificateWriter) error {
	if len(c.caData) > 0 {
		return writer.Write(
			c.metadata,
			bytes.Join([][]byte{c.certData, c.caData}, []byte("\n")),
			c.keyData,
		)
	}

	return writer.Write(c.metadata, c.certData, c.keyData)
}
