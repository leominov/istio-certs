package istiocerts

import (
	"testing"
	"time"
)

func TestNewCertificate(t *testing.T) {
	tests := []struct {
		certPath CertificatePath
		wantErr  bool
	}{
		{
			certPath: CertificatePath{
				Fqdn: "adm.tinkoff.ru",
				Path: "../../test_data/tls/pfa/adm_20190508",
			},
			wantErr: false,
		},
		{
			certPath: CertificatePath{
				Fqdn: "adm.tinkoff.ru",
				Path: "../../test_data/tls/pfa/adm_20190508_nocrt",
			},
			wantErr: true,
		},
		{
			certPath: CertificatePath{
				Fqdn: "adm.tinkoff.ru",
				Path: "../../test_data/tls/pfa/adm_20190508_nokey",
			},
			wantErr: true,
		},
		{
			certPath: CertificatePath{
				Fqdn: "adm.tinkoff.ru",
				Path: "../../test_data/tls/pfa/adm_20190508_noca",
			},
			wantErr: true,
		},
	}

	config := &Config{
		Source: "file",
	}
	provider, err := NewCertificateProvider(config)
	if err != nil {
		t.Fatalf("NewCertificateProvider() == %v", err)
	}
	for _, test := range tests {
		_, err = NewCertificate(provider, test.certPath)
		if (err != nil) != test.wantErr {
			t.Errorf("NewCertificate() error = %v, wantErr %v", err, test.wantErr)
		}
	}
}

func TestVerify(t *testing.T) {
	provider, err := NewCertificateProvider(&Config{Source: "file"})
	if err != nil {
		t.Fatalf("NewCertificateProvider() == %v", err)
	}
	cert := &Certificate{}
	_, err = cert.Verify()
	if err == nil {
		t.Error("Verify() == nil")
	}
	cert, err = NewCertificate(provider, CertificatePath{
		Fqdn: "adm.tinkoff.ru",
		Path: "../../test_data/tls/pfa/adm_20190508",
	})
	if err != nil {
		t.Fatalf("NewCertificate() == %v", err)
	}
	_, err = cert.Verify(WithTimeShift(5 * time.Minute))
	if err == nil {
		t.Error("Verify() == nil")
	}
}
