package istiocerts

type CertificateWriter interface {
	Write(metadata CertificateMetadata, public, private []byte) error
}
