package istiocerts

import (
	"os"
	"testing"
)

func TestNewCertificateProvider(t *testing.T) {
	os.Setenv("VAULT_AUTH_METHOD", "token")
	tests := []struct {
		config  *Config
		wantErr bool
	}{
		{
			config:  &Config{},
			wantErr: true,
		},
		{
			config: &Config{
				Source: "foobar",
			},
			wantErr: true,
		},
		{
			config: &Config{
				Source: "file",
			},
			wantErr: false,
		},
		{
			config: &Config{
				Source: "vault",
			},
			wantErr: false,
		},
	}
	for _, test := range tests {
		_, err := NewCertificateProvider(test.config)
		if (err != nil) != test.wantErr {
			t.Errorf("NewCertificateProvider() error = %v, wantErr %v", err, test.wantErr)
		}
	}
}
