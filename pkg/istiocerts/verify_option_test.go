package istiocerts

import (
	"crypto/x509"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestWithTimeShift(t *testing.T) {
	verifyOptions := &x509.VerifyOptions{}
	assert.True(t, verifyOptions.CurrentTime.IsZero())
	opt := WithTimeShift(24 * time.Hour)
	opt(verifyOptions)
	if assert.False(t, verifyOptions.CurrentTime.IsZero()) {
		assert.True(t, verifyOptions.CurrentTime.After(time.Now()))
	}

	now := time.Now()
	verifyOptions = &x509.VerifyOptions{
		CurrentTime: now,
	}
	opt = WithTimeShift(24 * time.Hour)
	opt(verifyOptions)
	assert.Equal(t, now.Add(24*time.Hour), verifyOptions.CurrentTime)
}
