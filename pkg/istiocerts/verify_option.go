package istiocerts

import (
	"crypto/x509"
	"time"
)

type VerifyOption func(*x509.VerifyOptions)

func WithTimeShift(d time.Duration) VerifyOption {
	return func(verifyOpts *x509.VerifyOptions) {
		if verifyOpts.CurrentTime.IsZero() {
			verifyOpts.CurrentTime = time.Now().Add(d)
		} else {
			verifyOpts.CurrentTime = verifyOpts.CurrentTime.Add(d)
		}
	}
}
