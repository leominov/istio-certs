# Istio Certs

Данная утилита создает сертификаты в kubernetes кластере для последующего использования.
Рекомендуется использовать собранный докер-образ `eu.gcr.io/utilities-212509/devops/istio-certs`.

Пример конфигурации

```yaml
source: vault

certificates:
- fqdn: "help.tinkoff.ru"
  path: "tls/tj/help.tinkoff.ru_20200814"
- fqdn: "*.tinkoffjournal.ru"
  path: "tls/tj/wildcard_tinkoffjournal.ru_20201001"
```

Для конфигурации Vault-клиента используются переменные окружения

```
VAULT_ADDR – Адрес сервера, например https://vault-rnd.tcsbank.ru
VAULT_MOUNT_POINT – Корневая директория, например advert
VAULT_AUTH_METHOD – Используемый метод аутентификации, поддерживается token, userpass и ldap
VAULT_TOKEN – API-токен, используется если в качестве метода аутентификации выбран token
VAULT_LOGIN – Имя пользователя, используется если в качестве метода аутентификации выбран userpass или ldap
VAULT_PASSWORD – Пароль, используется если в качестве метода аутентификации выбран userpass или ldap
```

Для конфигурации kubernetes-client используется kubeconfig, поэтому получить credentials для работы с кластером необходимо до запуска утилиты. 
Выбор namespace возможен с помощью флага.

Пример использования:

```bash
istio-certs create-secrets -f certificates.yaml -n istio-system
```
