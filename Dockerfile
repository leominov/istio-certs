ARG GO_VERSION=1.14
ARG ALPINE_VERSION=3.11

FROM golang:${GO_VERSION}-alpine${ALPINE_VERSION} as build
ARG ALPINE_VERSION
WORKDIR /$GOPATH/src/gitlab-rnd.tcsbank.ru/devops/k8s-tools/istio-certs
RUN apk add --no-cache git
ENV GO111MODULE=on
COPY . .
RUN go mod download
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -ldflags '-extldflags "-static"' -o /usr/local/bin/istio-certs

FROM eu.gcr.io/utilities-212509/devops/gcp-projects-ci:latest
COPY --from=build /usr/local/bin/istio-certs /usr/local/bin/istio-certs
